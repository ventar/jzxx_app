### 家长学校首页改版
```
.
├── README.md
├── addchild.html
├── css
│   ├── index.css
│   └── swiper-3.4.2.min.css
├── icomoon
│   ├── Read Me.txt
│   ├── demo-files
│   │   ├── demo.css
│   │   └── demo.js
│   ├── demo.html
│   ├── fonts
│   │   ├── icomoon.eot
│   │   ├── icomoon.svg
│   │   ├── icomoon.ttf
│   │   └── icomoon.woff
│   ├── selection.json
│   └── style.css
├── images
│   ├── 0-1708-1.jpg
│   ├── 0-1708-2.jpg
│   ├── 0-1708-3.jpg
│   ├── 0-1708-4.jpg
│   ├── 0-1708-5.jpg
│   ├── 0-3.png
│   ├── 1-1708-1.jpg
│   ├── 1-1708-2.jpg
│   ├── 1-1708-3.jpg
│   ├── 1-1708-4.jpg
│   ├── 1-1708-5.jpg
│   ├── 1-1708-6.png
│   ├── 1.png
│   ├── 100.png
│   ├── 104.png
│   ├── 111.png
│   ├── 119.png
│   ├── 121.png
│   ├── 122.png
│   ├── 1221.png
│   ├── 128.png
│   ├── 1280.png
│   ├── 130.png
│   ├── 2-1708-1.jpg
│   ├── 2-1708-2.jpg
│   ├── 2-1708-3.jpg
│   ├── 2-1708-4.jpg
│   ├── 2-1708-5.jpg
│   ├── 2-1708-6.jpeg
│   ├── 25.png
│   ├── 3-1708-1.jpg
│   ├── 3-1708-2.jpg
│   ├── 3-1708-3.jpg
│   ├── 3-1708-4.jpg
│   ├── 3-1708-5.jpg
│   ├── 3-1708-6.jpg
│   ├── 3-1708-7.jpg
│   ├── 3.png
│   ├── 313.png
│   ├── 4-1708-1.jpg
│   ├── 4-1708-2.jpg
│   ├── 4-1708-3.jpg
│   ├── 4-1708-4.jpg
│   ├── 4-1708-5.jpg
│   ├── 4-1708-6.jpg
│   ├── 5.png
│   ├── 7.png
│   ├── 84490701.png
│   ├── 8480852.png
│   ├── 97.png
│   ├── 97955023.png
│   ├── 98.png
│   ├── 99.png
│   ├── bg.png
│   ├── bgwe.png
│   ├── book.png
│   ├── circle1.png
│   ├── degree3.png
│   ├── gestation.png
│   ├── high_school.png
│   ├── house.png
│   ├── integral_task_bg.png
│   ├── jiaolv1.jpg
│   ├── jiaolv2.jpg
│   ├── junior_school.png
│   ├── k1.png
│   ├── k2.png
│   ├── kapai1.png
│   ├── kapai2.png
│   ├── kapai3.png
│   ├── kindergarten.png
│   ├── logo.png
│   ├── search_1.png
│   ├── select.png
│   ├── title_bg.png
│   ├── title_bg2.png
│   ├── user-01.png
│   ├── user_02.png
│   ├── user_03.png
│   ├── user_add.png
│   ├── xiaoxue.png
│   ├── yiyu1.jpg
│   ├── yiyu2.jpg
│   └── zhezhao.png
├── index.html
└── js
    ├── autoSize.js
    ├── jquery.js
    ├── main.js
    └── swiper-3.4.2.jquery.min.js

6 directories, 103 files
```