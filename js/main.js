/*
 * @Author: Ventar 
 * @Date: 2017-09-13 14:38:22 
 * @Last Modified by: Ventar
 * @Last Modified time: 2017-09-14 14:14:45
 */

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != "function") {
    window.onload = func
  } else {
    window.onload = function () {
      oldonload();
      func()
    }
  }
};

/* Tab切换 */
function EW_tab(option) {
  this.oTab_btn = this.getDom(option.tabBtn);
  this.oTab_clist = this.getDom(option.tabCon);
  if (!this.oTab_btn || !this.oTab_clist) return;
  this.sCur = option.cur;
  this.type = option.type || 'click';
  this.nLen = this.oTab_btn.length;
  this.int()
}
EW_tab.prototype = {
  getId: function (id) {
    return document.getElementById(id)
  },
  getByClassName: function (className, parent) {
    var elem = [],
      node = parent != undefined && parent.nodeType == 1 ? parent.getElementsByTagName('*') : document.getElementsByTagName('*'),
      p = new RegExp("(^|\\s)" + className + "(\\s|$)");
    for (var n = 0, i = node.length; n < i; n++) {
      if (p.test(node[n].className)) {
        elem.push(node[n])
      }
    }
    return elem
  },
  getDom: function (s) {
    var nodeName = s.split(' '),
      p = this.getId(nodeName[0].slice(1)),
      c = this.getByClassName(nodeName[1].slice(1), p);
    if (!p || c.length == 0) return null;
    return c
  },
  change: function () {
    var cur = new RegExp(this.sCur, 'g');
    for (var n = 0; n < this.nLen; n++) {
      this.oTab_clist[n].style.display = 'none';
      this.oTab_btn[n].className = this.oTab_btn[n].className.replace(cur, '')
    }
  },
  int: function () {
    var that = this;
    this.oTab_btn[0].className += ' ' + this.sCur;
    this.oTab_clist[0].style.display = 'block';
    for (var n = 0; n < this.nLen; n++) {
      this.oTab_btn[n].index = n;
      this.oTab_btn[n]['on' + this.type] = function () {
        that.change();
        that.oTab_btn[this.index].className += ' ' + that.sCur;
        that.oTab_clist[this.index].style.display = 'block'
      }
    }
  }
};

// GoTop返回顶部
function toTop(id, show) {
  var oTop = document.getElementById(id);
  var bShow = show;
  if (!bShow) {
    oTop.style.display = 'none';
    setTimeout(btnShow, 50)
  }
  oTop.onclick = scrollToTop;

  function scrollToTop() {
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    var iSpeed = Math.floor(-scrollTop / 2);
    if (scrollTop <= 0) {
      if (!bShow) {
        oTop.style.display = 'none'
      }
      return
    }
    document.documentElement.scrollTop = document.body.scrollTop = scrollTop + iSpeed;
    setTimeout(arguments.callee, 50)
  }

  function btnShow() {
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    if (scrollTop <= 0) {
      oTop.style.display = 'none'
    } else {
      oTop.style.display = 'block'
    }
    setTimeout(arguments.callee, 50)
  }
};

// 角色选择弹窗

function popUp() {
  document.getElementById("popBg").style.display = "block";
  document.getElementById("popNav").style.display = "block";
  document.body.style.overflow = "hidden";
  $(window).scroll(function () {
    $(this).scrollTop(0)
  });
  $(document).bind("touchmove", function (e) {
    e.preventDefault();
  });
}

function popDown() {
  document.getElementById("popBg").style.display = "none";
  document.getElementById("popNav").style.display = "none";
  document.body.style.overflow = "";
  $(window).unbind("scroll");
  $(document).unbind("touchmove");
}

// 签到弹窗

function popUpQd() {
  document.getElementById("popBg").style.display = "block";
  document.getElementById("popNavQd").style.display = "block";
  document.body.style.overflow = "hidden";
  $(window).scroll(function () {
    $(this).scrollTop(0)
  });
  $(document).bind("touchmove", function (e) {
    e.preventDefault();
  });
}

function popDownQd() {
  document.getElementById("popBg").style.display = "none";
  document.getElementById("popNavQd").style.display = "none";
  document.body.style.overflow = "";
  $(window).unbind("scroll");
  $(document).unbind("touchmove");
}

// 更新弹窗

function popUpGx() {
  document.getElementById("popBg").style.display = "block";
  document.getElementById("popNavGx").style.display = "block";
  document.body.style.overflow = "hidden";
  $(window).scroll(function () {
    $(this).scrollTop(0)
  });
  $(document).bind("touchmove", function (e) {
    e.preventDefault();
  });
}

function popDownGx() {
  document.getElementById("popBg").style.display = "none";
  document.getElementById("popNavGx").style.display = "none";
  document.body.style.overflow = "";
  $(window).unbind("scroll");
  $(document).unbind("touchmove");
}